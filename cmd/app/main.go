package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"log"
	"net/http"
	"review/internal/controllers"
	"review/internal/storage/postgres"
)

func main() {
	storage := postgres.NewStoragePostgres()
	userController := controllers.NewUserController(storage)

	router := chi.NewRouter()

	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	router.Post("/users", userController.CreateUser)
	router.Get("/users/{userID}", userController.GetUserByID)
	router.Put("/users/{userID}", userController.UpdateUser)
	router.Delete("/users/{userID}", userController.DeleteUser)

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		log.Fatal(err)
	}
}
