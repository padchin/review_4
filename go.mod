module review

go 1.20

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.2
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.9
)

require github.com/ajg/form v1.5.1 // indirect
