package controllers

import (
	"encoding/json"
	"net/http"
	"review/internal/models"
	"review/internal/storage/postgres"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type UserController struct {
	storage *postgres.StoragePostgres
}

func NewUserController(storage *postgres.StoragePostgres) *UserController {
	return &UserController{storage: storage}
}

func (c *UserController) CreateUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, map[string]string{"error": "Invalid request payload"})
		return
	}

	if err := c.storage.CreateUser(&user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		render.JSON(w, r, map[string]string{"error": "Failed to create user"})
		return
	}

	w.WriteHeader(http.StatusCreated)
	render.JSON(w, r, user)
}

func (c *UserController) GetUserByID(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "userID")
	id, err := strconv.Atoi(userID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, map[string]string{"error": "Invalid user ID"})
		return
	}

	user, err := c.storage.GetUserByID(id)
	if err != nil {
		if err == models.ErrUserNotFound {
			w.WriteHeader(http.StatusNotFound)
			render.JSON(w, r, map[string]string{"error": "User not found"})
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		render.JSON(w, r, map[string]string{"error": "Failed to get user"})
		return
	}

	render.JSON(w, r, user)
}

func (c *UserController) UpdateUser(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "userID")
	id, err := strconv.Atoi(userID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, map[string]string{"error": "Invalid user ID"})
		return
	}

	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, map[string]string{"error": "Invalid request payload"})
		return
	}
	user.ID = id

	if err := c.storage.UpdateUser(&user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		render.JSON(w, r, map[string]string{"error": "Failed to update user"})
		return
	}

	render.JSON(w, r, user)
}

func (c *UserController) DeleteUser(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "userID")
	id, err := strconv.Atoi(userID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		render.JSON(w, r, map[string]string{"error": "Invalid user ID"})
		return
	}

	if err := c.storage.DeleteUser(id); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		render.JSON(w, r, map[string]string{"error": "Failed to delete user"})
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
