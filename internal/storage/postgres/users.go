package postgres

import (
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
	"review/internal/models"
)

type StoragePostgres struct {
	db *sqlx.DB
}

func connectDB() (*sqlx.DB, error) {
	connStr := "postgres://postgres:password@localhost:5432/postgres?sslmode=disable"

	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}

func NewStoragePostgres() *StoragePostgres {
	db, err := connectDB()
	if err != nil {
		log.Fatal(err)
	}

	return &StoragePostgres{db: db}
}

func (s *StoragePostgres) CreateUser(user *models.User) error {
	query := `INSERT INTO users (id, first_name, last_name, age, email, phone, address, city, country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`
	_, err := s.db.Exec(query, user.ID, user.FirstName, user.LastName, user.Age, user.Email, user.Phone, user.Address, user.City, user.Country)
	if err != nil {
		return err
	}

	return nil
}

func (s *StoragePostgres) GetUserByID(id int) (*models.User, error) {
	query := `SELECT * FROM users WHERE id = $1`
	var user models.User
	err := s.db.Get(&user, query, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, models.ErrUserNotFound
		}
		return nil, err
	}
	return &user, nil
}

func (s *StoragePostgres) UpdateUser(user *models.User) error {
	query := `UPDATE users SET first_name = $1, last_name = $2, age = $3, email = $4, phone = $5, address = $6, city = $7, country = $8 WHERE id = $9`
	_, err := s.db.Exec(query, user.FirstName, user.LastName, user.Age, user.Email, user.Phone, user.Address, user.City, user.Country, user.ID)
	if err != nil {
		return err
	}
	return nil
}

func (s *StoragePostgres) DeleteUser(id int) error {
	query := `DELETE FROM users WHERE id = $1`
	_, err := s.db.Exec(query, id)
	if err != nil {
		return err
	}
	return nil
}
